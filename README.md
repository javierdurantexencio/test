# test repo

This is an empty repo to use as a workplace for an interview exercise.
To complete this exercise, please create a git repo in the provider of your choice (Gitlab, Github, BitBucket)
We just need to know the URL as soon as it is created and need to have read permissions on it so ideally it should be a public repo

We expect the candidate to treat this exercise as a project so we expect multiple commits, with descriptive descriptions
Not a single commit with the whole project working

# Exercise

The goal of this exercise is to create a very simple frontend app to see
how the candidate works with Angular features and folder structure.

The user of the app will have a CSV file with the following structure:

| date | amount | summary |
| ------ | ------ | ------ |
| 2019.01.01 | -1000 | rent |
| 2019.01.02 | -1400 | AWS Marketplace |
| 2019.01.03 | 5000 | Salary |
| 2019.01.03 | 1000 | Uni Loan |
...

Candidate should create a csv file with that structure and enough data so the features we want to show in the app are visible


When the user enters the app, will be presented with a Wizard:

- First page: Let the user Upload a CSV file
- Second page: Preview the CSV file and show an Input element to enter the initial Balance (this must be a number, could be negative). At the bottom, there should be Confirm or Cancel options (this goes back to the first page)

Once the user Confirms the file and the balance:

- User should arrive to a page with two main elements
1. A chart showing the balance line day by day with the data coming from the uploaded file, starting from the initial balance set in the second page of the wizard
2. A table showing all the records of the uploaded file. Amounts on this records should be editable, and any changes to these amounts should be reflected on the chart

# Considerations

- No backend should be needed to complete this app, so the file uploaded has to be handled by the frontend only
- Usability of the app will be evaluated as well, although we are not expecting a perfect app, this should be somewhat professional
- The way this app will be tested is by entering a set of data that the candidate doesn't know so we can test validation scenarios, usability and responsiveness of the app
- Ideally, the app should be done as mobile first, but is not explicity required to be responsive

# Tech specs

- Framework to use should be Angular 7 or higher
- A chart library of your choice, like eCharts, Chart.js, D3, etc
- Any CSS/JS library can be used if it helps to speed up the development and improve the UX. Examples of this are Bootstrap, Ant Design, Material Design, etc

